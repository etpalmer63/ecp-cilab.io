# ECP-CI Documentation pages

This project hosts ECP CI documentation at https://ecp-ci.gitlab.io/index.html 
built using [GitLab Page w/Sphinx](https://gitlab.com/pages/sphinx).

## Locally build documentation

* Install [Python](https://www.python.org/downloads/) and [pip](https://pip.pypa.io/en/stable/installing/)
* Change directories to your local checkout

  ```shell
  cd <local repository checkout>
  ```

* Install required dependencies

  ```shell
  pip install -r requirements.txt
  ```

* Build command

  ```shell
  make html
  ```

### Results

* Generated in `_build/`
* Index found at `_build/html/index.html` and be opened in a local browser.

## Contributing

For contributing to this project see CONTRIBUTING.md

### Style Checker

In addition to ensuring that you are able to locally build your documentation we
utilize [doc8](https://pypi.org/project/doc8) style checker. Simply `pip install doc8`
and from the project directory:

  ```console
  $ doc8
  Scanning...
  Validating...
  ========
  Total files scanned = 32
  ```

## Attributions

* Diagrams created using
  [draw.io by JGrpah](https://github.com/jgraph/drawio)
  with provided icons/templates licensed under
  [CC BY-SA-4.0](https://creativecommons.org/licenses/by-sa/4.0)
