# Contributing Guide

This guide will discuss the contribution process for ECP-CI docs. You may
want to familiarize yourself with [git flow model](https://guides.github.com/introduction/flow/) 
if you are new to git.

## Fork repo

First click the **Fork** button on top-right corner, this will create a fork of
upstream repo in your namespace, you should have a namespace
``https://gitlab.com/<gitlab-username>/ecp-ci.gitlab.io``.

Next clone this repo in your local machine

* HTTPS: ``git clone https://gitlab.com/<gitlab-username>/ecp-ci.gitlab.io``
* SSH: ``git clone https://gitlab.com/<gitlab-username>/ecp-ci.gitlab.io.git``.

SSH will be convenient so that you don't have to type user authentication upon
git push. If you haven't setup SSH authentication, please see:
https://docs.gitlab.com/ee/gitlab-basics/create-your-ssh-keys.html

## Setup 

If you have cloned your repo, navigate to the repo and check your remote.
Your `origin` should be your forked repo.

```
$ cd ecp-ci.gitlab.io 
$ git remote -v
origin	git@gitlab.com:<gitlab-username>/ecp-ci.gitlab.io.git (fetch)
origin	git@gitlab.com:<gitlab-username>/ecp-ci.gitlab.io.git (push)
```

You will want to add the upstream repo ``https://gitlab.com/ecp-ci/ecp-ci.gitlab.io``
in git in order to keep your fork in sync. To add the upstream repo let's add
an ``upstream`` remote as follows

```
$ git remote add upstream https://gitlab.com/ecp-ci/ecp-ci.gitlab.io
```

Now we should see two remote endpoints. 

```shell
$ git remote -v
origin	git@gitlab.com:<gitlab-username>/ecp-ci.gitlab.io.git (fetch)
origin	git@gitlab.com:<gitlab-username>/ecp-ci.gitlab.io.git (push)
upstream	https://gitlab.com/ecp-ci/ecp-ci.gitlab.io (fetch)
upstream	https://gitlab.com/ecp-ci/ecp-ci.gitlab.io (push)
```

To sync your local `main` branch which is your default branch you can do
the following

```shell
git pull upstream main
```

## Building Documentation 

The core of the documentation efforts are built with [Sphinx](sphinx-doc.org). If
you are unfamiliar with it or reStructedText there are a several resources we
advised reviewing:

* [reStructuredText Primer](http://docutils.sourceforge.net/docs/user/rst/quickref.html)
* [Sphinx Documentation](http://www.sphinx-doc.org/en/stable/contents.html)
* [rst-cheatsheet](https://github.com/ralsina/rst-cheatsheet/blob/master/rst-cheatsheet.rst)

It is recommended you have an instance of [Python](https://www.python.org/downloads/)
and [pip](https://pip.pypa.io/en/stable/installing/). You may
want to setup a python environment if you have any preference here are few suggestions
 
 - [virtualenv](https://virtualenv.pypa.io/en/latest/)
 - [pipenv](https://pipenv.pypa.io/en/latest/)
 - [conda](https://docs.conda.io/en/latest/)

Navigate to the repo and install python dependencies for this project

```shell
$ cd ecp-ci.gitlab.io 
$ pip install -r requirements.txt
```

Next we build the sphinx docs by running:

```shell
make html
```

The project will be built under `_build` directory, you can open the webpage by running
or point your browser to the full path to `index.html`.

```
open _build/html/index.html
```

## Feature Request Process

In order to contribute back to docs, first navigate to your local
`main` branch and make sure its in sync with upstream

```shell
git checkout main
git pull upstream main
```

Next create a feature branch, name it something meaningful

```
git checkout -b update_contributing_docs
```

Alternately, you may want to create a feature branch based on a gitlab issue that can be useful when to address an open issue. For instance if your branch address issue `#16` you can
name your branch as follows

```
git checkout -b _issue_16
```

Then make your changes as necessary and add, commit and push your files as follows

```
git add <file1> <file2>
git commit
git push origin
```

Please make sure your git author has your name and email address tied to your gitlab account. This shows up in the commit
something as follows

```
Author: First Last <email>
```

To configure your git author see https://git-scm.com/book/en/v2/Getting-Started-First-Time-Git-Setup. Once you have configured your git author you can check the configuration by 
running

```shell
$ git config user.name

$ git config user.email
```


**Please do not make changes to your local `main` branch because this branch is used to sync your branch with upstream.**

**Do not push your changes to upstream default branch ``git push upstream main``, it's protected branch and we recommend you send a MR at https://gitlab.com/ecp-ci/ecp-ci.gitlab.io/-/merge_requests**

Once you have pushed your changes to your upstream fork next step is to create a Merge Request to upstream project at https://gitlab.com/ecp-ci/ecp-ci.gitlab.io/-/merge_requests. If you are 
unsure how to create Merge Request in Gitlab see https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html 
