Administration
==============

.. _introduction: introduction.html#enhancements
.. _official documentation: https://docs.gitlab.com

Documentation to support administrating runners and servers targeting HPC
environments. The information provided here is meant to supplement the
`official documentation`_ in areas where it may
not address HPC focused questions while also providing comprehensive details
on all ECP CI related enhancements and applications. For details on using the
runners themselves please see the `Project Developer <ci-users.html>`_
documentation or your site local resources.

Jacamar CI
----------

Jacamar is a HPC focused CI/CD driver that work's in the
`GitLab custom executor model <https://docs.gitlab.com/runner/executors/custom.html>`_.
The core goal of this project is to establish a maintainable,
yet extensively configurable tool that will allow for the use of GitLab’s
robust CI model on unique HPC test resources. Allowing code teams to
integrate potentially existing pipelines on powerful scientific development
environments.

.. toctree::
   :maxdepth: 1

   admin/jacamar/introduction.rst
   admin/jacamar/tutorial.rst
   admin/jacamar/deployment.rst
   admin/jacamar/configuration.rst
   admin/jacamar/auth.rst
   admin/jacamar/executors.rst
   admin/jacamar/troubleshoot.rst

Server
------

`GitLab <https://about.gitlab.com/what-is-gitlab/>`_ offers a
comprehensive development platform all within a single application. A major
benefit to using this tool is its both predominately
`open source <https://gitlab.com/gitlab-org/gitlab>`_ and allows for
management of self-hosted instances. However, there are a
`number of features <https://about.gitlab.com/pricing/self-managed/feature-comparison/>`_
that necessitate the purchase of a license.

All documentation organized here is not written to replace any exiting
`official documentation`_ offered. We only seek to highlight
specifics that are directly related to HPC centers.

.. toctree::
   :maxdepth: 2

   admin/server-admin.rst

Guides
------

Highlights best practices as well as potential workflows you may
wish to leverage when supporting ECP CI on facility test resources.

.. list-table::
    :header-rows: 1
    :widths: 15, 7, 7, 25

    * - Title
      - Jacamar CI
      - Server
      - Description
    * - `Non-Root Jacamar CI Downscoping (with Capabilities) via SetUID <guides/non-root-deployment-setuid.html>`_
      - √
      - x
      - How to deploy, configure, and run Jacamar CI with *setuid* downscoping as a non-root user.
    * - `Non-Root Jacamar CI Downscoping via Sudo <guides/non-root-deployment-sudo.html>`_
      - √
      - x
      - How to deploy, configure, and run Jacamar CI with *sudo* downscoping as a non-root user.
    * - `Tracing CI Jobs with Administrative Tools <guides/admin-job-tracing.html>`_
      - √
      - x
      - Using recommended configurations for Jacamar CI along with server logs to proper trace job execution.
    * - `Seccomp Plugin Support - Introduction <guides/seccomp-plugin-intro.html>`_
      - √
      - x
      - Introduction to using Golang plugins to create deployment specific seccomp filters.
    * - `Configuring and Troubleshooting Seccomp <guides/seccomp-jacamar-administration.html>`_
      - √
      - x
      - Details on how Seccomp can be configured to meet deployment requirements.

.. toctree::
   :maxdepth: 1
   :hidden:

   guides/non-root-deployment-setuid.rst
   guides/non-root-deployment-sudo.rst
   guides/admin-job-tracing.rst
   guides/seccomp-plugin-intro.rst
   guides/seccomp-jacamar-administration.rst

Latest Releases
---------------

Notes for all releases can be found `here <releasenotes/all.html>`_.

.. toctree::
   :maxdepth: 1

   releasenotes/jacamar/jacamar_0.13.0.rst
   releasenotes/jacamar/jacamar_0.12.1.rst
