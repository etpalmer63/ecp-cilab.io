Jacamar CI v0.9.1
=================

* *Release*: `v0.9.1 <https://gitlab.com/ecp-ci/jacamar-ci/-/releases/v0.9.1>`_
* *Date*: 12/13/2021

.. important::

    In addition to this update please be advised of the runner security
    `release 14.5.2 <https://about.gitlab.com/releases/2021/12/10/security-release-gitlab-runner-14-5-2-released/>`_.
    If the issue scope affects your deployment please update both
    applications.

Bug & Development Fixes
-----------------------

* Upgraded to Go version `1.17.5`
  (`!303 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/303>`_)
