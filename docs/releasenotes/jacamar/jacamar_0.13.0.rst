Jacamar CI v0.13.0
==================

* *Release*: `v0.13.0 <https://gitlab.com/ecp-ci/jacamar-ci/-/releases/v0.13.0>`_
* *Date*: 12/22/2022

User Changes
------------

* Job command notification for all batch executors
  (`!406 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/406>`_)

  - .. code-block:: bash

      Executing "step_script" stage of the job script
      Slurm job command: sbatch --wait --job-name=ci-456_1671728223
        --output=/home/user/.jacamar-ci/scripts/abc123/000/user/test/456/slurm-ci-456.out
        -N1 -A account1 -t 00:30:00
        /home/user/.jacamar-ci/scripts/abc123/000/user/test/456/build_script.bash
      ...

  - These notifications will appear in your CI job logs and are meant to
    assist in troubleshooting failed ``sbatch`` submissions.

Admin Changes
-------------

* New executor option for the `Flux <http://flux-framework.org/>`_ resource manager
  (`!396 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/396>`_) - (Thanks ``@alecbcs``)

  - An exciting contribution that offers a new
    `Supported Executor <https://ecp-ci.gitlab.io/docs/admin/jacamar/executors.html>`_
    for deployment leveraging the Flux scheduler by submitting
    jobs using ``flux mini alloc``.

  - .. code-block:: toml

      [general]
      executor = "flux"

* Additional logging for RunAs validation scripts
  (`!401 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/401>`_)

  - When `system logging <https://ecp-ci.gitlab.io/docs/guides/admin-job-tracing.html>`_
    is enabled these additional details are meant to assist in troubleshooting
    errors that may be associated with a
    `RunAs User <https://ecp-ci.gitlab.io/docs/admin/jacamar/auth.html#runas-user>`_
    ``validation_script``.

* Optional group permission (``750``) configuration for ``data_dir``:
  (`!372 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/372>`_,
  `!386 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/386>`_,
  `!414 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/414>`_)

  - When enabled this will create all standard directories with group
    read/execute permission. This is based upon the default GID for the
    job user and is meant to ideally be used in conjunction with
    ``RunAs``. Please note that any files/folders created during the CI
    job will have permissions that align with the users' default
    `umask <https://man7.org/linux/man-pages/man2/umask.2.html>`_.

  - .. code-block:: toml

      [general]
      group_permissions = true

* Feature flag for additional ``sacct`` check for the Slurm executor
  (`!409 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/409>`_)

  - In limited cases external event that cause the cancellation of a Slurm
    job will not results in a non-zero exit code that can be observed by
    ``sbatch --wait ...``. This can lead to unexpectedly passing CI/CD jobs.

  - .. code-block:: toml

      [batch]
      ff_slurm_sacct = true

  - .. note::

      This option will remain behind a feature flag until more testing can
      be accomplished across target deployments. Please notify us of any issues
      encountered if testing this feature.

Bug & Development Fixes
-----------------------

* Upgraded to Go version *1.19.3* and dependencies
  (`!404 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/404>`_,
  `!410 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/410>`_,
  `!391 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/391>`_)
* Updated Slurm test image
  (`!407 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/407>`_)
* Job status reporting updated to align with documented examples
  (`!403 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/403>`_)

  - For details please see the
    `Report Build Status to GitLab/GitHub <https://ecp-ci.gitlab.io/docs/guides/build-status-gitlab.html>`_
    guide.

* Add Go vulnerability checking
  (`!395 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/395>`_)

  -  Leverages the newly released `govulncheck <https://pkg.go.dev/golang.org/x/vuln/cmd/govulncheck>`_
     in conjunction with a local application to enforce rules and allow for
     ignoring unrelated vulnerabilities.

  - .. code-block::

        # Defined in: tools/vulnerability-check/ignored-vulns.json
        "ignored-vulns": [ {
          "id": "GO-2022-1143",
          "aliases": ["CVE-2022-41720"],
          "reason": "We do no support Windows deployments.",
          "references": [
            {"type": "FIX","url": "https://go.dev/cl/455716"}
          ]
        }]

* Improvements to supported Pavilion test series
  (`!397 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/397>`_,
  `!400 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/400>`_,
  `!410 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/410>`_)
* Relocated ``AbstractExecutor`` and related packages to allow imports
  (`!385 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/385>`_)
