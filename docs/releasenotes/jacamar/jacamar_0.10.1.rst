Jacamar CI v0.10.1
==================

* *Release*: `v0.10.1 <https://gitlab.com/ecp-ci/jacamar-ci/-/releases/v0.10.1>`_
* *Date*: 2/21/2022

.. note::

    Release *v0.9.0* relocated all RPM installed binaries into a single
    location, ``/opt/jacamar/bin``. This will offer a better standard moving
    forward, please be aware of this when upgrading from an older version.

Admin Changes
-------------

* Move ``setuid``/``setgid`` Seccomp to ``limit_setuid`` config option
  (`!323 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/323>`_)

  - By default restricting operations to only the authorized user's
    ``UID`` + ``GID`` can break workflows involving select schedulers and
    deployments in unexpected ways. As such we are making this
    optional instead.

  - .. code-block:: toml

        [auth.seccomp]
        limit_setuid = true
