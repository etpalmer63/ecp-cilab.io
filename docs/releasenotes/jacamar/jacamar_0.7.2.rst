.. _`configuration documentation`: ../../admin/jacamar/configuration.html#auth-seccomp-table

Jacamar CI v0.7.2
=================

* *Release*: `v0.7.2 <https://gitlab.com/ecp-ci/jacamar-ci/-/releases/v0.7.2>`_
* *Date*: 06/18/2021

Bug & Development Fixes
-----------------------

* Improved conditional seccomp filters used with ``setuid`` downscoping
  (`!198 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/196>`_).

  - This change will avoid issue presented by previous implementation
    that was overly restrictive of the ``ioctl`` system call leading
    to failed Python interactions.

  - For details on the use of the ``seccomp`` please see the related
    `configuration documentation`_.

* Iterative expansion of info/debug logs to better step through key
  actions of the ``jacamar-auth`` application
  (`!195 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/195>`_).
