:orphan:

All Releases
============

Detailed release notes for all runner and server development efforts.

.. toctree::
   :maxdepth: 1
   :caption: Jacamar CI

   jacamar/jacamar_0.13.0.rst
   jacamar/jacamar_0.12.1.rst
   jacamar/jacamar_0.12.0.rst
   jacamar/jacamar_0.11.0.rst
   jacamar/jacamar_0.10.2.rst
   jacamar/jacamar_0.10.1.rst
   jacamar/jacamar_0.10.0.rst
   jacamar/jacamar_0.9.1.rst
   jacamar/jacamar_0.9.0.rst
   jacamar/jacamar_0.8.2.rst
   jacamar/jacamar_0.8.1.rst
   jacamar/jacamar_0.8.0.rst
   jacamar/jacamar_0.7.3.rst
   jacamar/jacamar_0.7.2.rst
   jacamar/jacamar_0.7.1.rst
   jacamar/jacamar_0.7.0.rst
   jacamar/jacamar_0.6.0.rst
   jacamar/jacamar_0.5.0.rst
   jacamar/jacamar_0.4.2.rst
   jacamar/jacamar_0.4.1.rst
   jacamar/jacamar_0.4.0.rst
   jacamar/jacamar_0.3.2.rst
   jacamar/jacamar_0.3.1.rst
   jacamar/jacamar_0.3.0.rst
   jacamar/jacamar_0.2.0.rst
   jacamar/jacamar_0.1.0.rst

.. important::

   All development efforts have been shifted to supporting
   our implementation of a
   `custom executor driver (Jacamar CI) <https://gitlab.com/ecp-ci/jacamar-ci>`_.

.. toctree::
   :maxdepth: 1
   :caption: Runner (fork w/ECP Enhancements)

   runner/runner_13.0.1_0.6.1.rst
   runner/runner_13.0.0_0.6.1.rst
   runner/runner_12.7.0_0.6.1.rst
   runner/runner_12.7.0_0.6.0.rst
   runner/runner_12.6.0_0.5.4.rst
   runner/runner_12.6.0_0.5.3.rst
   runner/runner_12.4.0_0.5.2.rst
   runner/runner_12.4.0_0.5.1.rst
   runner/runner_12.4.0_0.5.0.rst
   runner/runner_12.0.0_0.4.3.rst
