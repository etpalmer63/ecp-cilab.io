Project Developer
=================

.. _enhancements: ../index.html#enhancements
.. _administrator documentation: admin.html
.. _official documentation: https://docs.gitlab.com

Documentation, best practices, and tutorials that are focused on making the
most out of HPC resources via ECP CI enhancements. As such, information
presented here is centred on those HPC focused enhancements_ made to
to GitLab. We will make ample reference to GitLab's `official documentation`_
and strongly encourage you to look there for details not covered in these
pages. For any questions on runner or server deployment please see the
`administrator documentation`_.

Continuous Integration
----------------------

Understanding the `GitLab Runner <https://gitlab.com/gitlab-org/gitlab-runner>`_
is the key to making full use of the GitLab CI ecosystem. These sections
are targeted not just at those unfamiliar with using the GitLab runner but
anyone who wishes to understand how the ECP CI enhancements_ will
affect their CI jobs.

.. toctree::
   :maxdepth: 2

   ci-users/ci-general.rst
   ci-users/ci-batch.rst
   ci-users/ci-faq.rst

Guides
------

Guides are written to highlight general recommendations as well as
potential workflows you may want to leverage when using CI resources
leveraging ECP specific enhancements.

.. list-table::
    :header-rows: 1
    :widths: 15, 30

    * - Title
      - Description
    * - `Report Build Status to GitLab/GitHub <guides/build-status-gitlab.html>`_
      - Leverage supported APIs in conjunction with a custom script to automate pipeline reporting to an upstream repository.
    * - `Multiple GitLab Project CI Structure <guides/multi-gitlab-project.html>`_
      - Optional configurations that can assist when managing GitLab CI/CD pipelines across multiple servers.
    * - `Artifacts, Caching, and Local Storage for HPC Projects <guides/hpc-artifacts-caching.html>`_
      - A generic look at the differences between artifacts, caching, and local storage for CI on test HPC resources.
    * - `Manual Directory Cleanup <guides/manual-directory-cleanup.html>`_
      - Example job and resources that can assist in cleaning up stateful resources generated during CI.

.. toctree::
   :maxdepth: 1
   :hidden:

   guides/build-status-gitlab.rst
   guides/multi-gitlab-project.rst
   guides/hpc-artifacts-caching.rst
   guides/manual-directory-cleanup.rst

Tutorials
---------

.. list-table::
    :header-rows: 1
    :widths: 15, 20

    * - Title
      - Description
    * - `ECP CI Startup Tutorial <tutorial/startup_tutorial.html>`_
      - Comprehensive beginner level introduction to CI and ECP supported resources.
    * - `MPI Quick Start <tutorial/quickstart.html>`_
      - Building and running a basic MPI application across compute resources.
    * - `2020 ECP Annual <tutorial/ecp_ci_startup.html>`_
      - Provided at the 2020 ECP Annual (**Deprecated**, please only use for reference).

ECP CI tutorials aim to provide a look at how one might use traditional
GitLab functionally coupled with the ECP CI enhancements and available
test resources in a self-paced manner. If your unsure how to get started
using these tutorials this will provide immense value.

.. toctree::
   :maxdepth: 1
   :hidden:

   tutorial/startup_tutorial.rst
   tutorial/quickstart.rst
   tutorial/ecp_ci_startup.rst
